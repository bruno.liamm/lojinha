-- MariaDB dump 10.17  Distrib 10.5.5-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: lojinha
-- ------------------------------------------------------
-- Server version	10.5.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produto` varchar(50) NOT NULL DEFAULT '',
  `preco` decimal(10,2) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `produto` (`produto`)
) ENGINE=InnoDB AUTO_INCREMENT=2068 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (2020,'SmartPhone A30 5G Samsung',1000.00,'Eletronico'),(2021,'SmartPhone A30s 5G Samsung',120.20,'Eletronico'),(2022,'Samsung 6s',829.70,'eletronico'),(2023,'Dell Redmi8',824.25,'eletronico'),(2024,'Samsung G9',321.94,'eletronico'),(2025,'Lg A30s',466.74,'eletronico'),(2026,'Samsung A30s',988.49,'eletronico'),(2027,'Motorola K9',268.12,'eletronico'),(2028,'Lg G9',721.98,'eletronico'),(2029,'Iphone K9',173.31,'eletronico'),(2030,'Motorola Inspiron 14R 3260',429.76,'eletronico'),(2031,'Samsung Galaxy J8',100.30,'eletronico'),(2032,'Samsung Aspire 3 A315-34 4GB',43.47,'eletronico'),(2033,'Xiome Inspiron 14R 3260',452.80,'eletronico'),(2034,'Xiome K9',552.70,'eletronico'),(2035,'Acer Redmi8',253.97,'eletronico'),(2036,'Acer A30s',412.61,'eletronico'),(2037,'Motorola Galaxy J8',130.65,'eletronico'),(2038,'Iphone Redmi8',651.40,'eletronico'),(2039,'Iphone A30s',351.87,'eletronico'),(2040,'Acer K9',206.40,'eletronico'),(2041,'Lg Redmi8',682.56,'eletronico'),(2042,'Lg 6s',463.60,'eletronico'),(2043,'Motorola G9',393.29,'eletronico'),(2044,'Xiome A30s',881.94,'eletronico'),(2045,'Xiome Galaxy J8',732.56,'eletronico'),(2046,'Dell 6s',793.53,'eletronico'),(2047,'Dell A30s',48.60,'eletronico'),(2048,'Dell Inspiron 14R 3260',935.92,'eletronico'),(2049,'Motorola Redmi8',983.62,'eletronico'),(2050,'Dell G9',626.41,'eletronico'),(2051,'Motorola A30s',654.45,'eletronico'),(2052,'Acer Inspiron 14R 3260',920.40,'eletronico'),(2053,'Iphone Aspire 3 A315-34 4GB',326.50,'eletronico'),(2054,'Dell Galaxy J8',780.31,'eletronico'),(2055,'Motorola 6s',506.57,'eletronico'),(2056,'Samsung K9',341.95,'eletronico'),(2057,'Motorola Aspire 3 A315-34 4GB',509.37,'eletronico'),(2058,'Acer Aspire 3 A315-34 4GB',644.34,'eletronico'),(2059,'Lg Galaxy J8',482.29,'eletronico'),(2060,'Xiome Redmi8',615.71,'eletronico'),(2061,'Dell K9',164.70,'eletronico'),(2062,'Lg K9',809.60,'eletronico'),(2063,'Acer 6s',211.69,'eletronico'),(2064,'Dell Aspire 3 A315-34 4GB',754.65,'eletronico'),(2065,'Iphone Galaxy J8',859.68,'eletronico'),(2066,'Lg Aspire 3 A315-34 4GB',638.27,'eletronico'),(2067,'Xiome Aspire 3 A315-34 4GB',973.10,'eletronico');
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Zeze','zeze@zeze.com','123456'),(2,'zezinho','emailzezinho@gmail.com','123456789');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15 15:26:35
