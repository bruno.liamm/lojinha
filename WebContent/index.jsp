<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="model.Usuario" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	//Verifica se existe uma sess�o
	Usuario user = session.getAttribute("usuario") != null ? (Usuario)session.getAttribute("usuario"): new Usuario();
	//Verifica se a pagina de login retornou algum erro
	int erro = request.getAttribute("erro") != null ? (int)request.getAttribute("erro") : 0;
	
	//Coloca a sessao e o erro como atributos da pagina
	pageContext.setAttribute("usuario", user);
	pageContext.setAttribute("erro", erro);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lojinha</title>
</head>
<body>
	<!-- Verifica se a sess�o est� ativa -->
	<c:if test="${usuario.id > 0}">
		<c:redirect url="items.jsp"></c:redirect>
	</c:if>
	
	<!-- Mostra o erro -->
	<c:choose>
		<c:when test="${erro == 1}">
			<p>Sua senha ou e-mail da conta est� incorreto.<p>
		</c:when>
		<c:when test="${erro == 2}">
			<p>Preencha os campos abaixo.<p>
		</c:when>
	</c:choose>
	
	<form action="login" method="post">
		<input type="email" name="email" placeholder="Digite seu e-mail" /> <input
			type="password" name="senha" placeholder="Digite sua senha" /> <input
			type="submit" name="logar" value="Logar">
	</form>
</body>
</html>