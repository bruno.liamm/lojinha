<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*, controle.ProdutoController, java.util.List, java.util.Random, java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
	//Verifica se existe uma sess�o
	Usuario user = session.getAttribute("usuario") != null ? (Usuario)session.getAttribute("usuario"): new Usuario();
	//Pega todos os produtoss
	List<Produto> produtos = new ProdutoController().selecionarPodutos();
	
	//Coloca a sessao e os produtos como atributos da pagina
	pageContext.setAttribute("produtos", produtos);
	pageContext.setAttribute("usuario", user);
%>
<fmt:setLocale value="pt_BR"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Lista de produtos</title>
</head>
<body>
	<!-- Verifica se a sess�o est� ativa -->
	<c:choose>
		<c:when test="${usuario.id > 0}">
			<span>Bem vindo, <c:out value="${usuario.nome}"></c:out>!</span>
			<a href="login?logout">Sair</a>
		</c:when>
		<c:otherwise>
			<c:redirect url="index.jsp"></c:redirect>
		</c:otherwise>
	</c:choose>
	
	<table>
		<thead>
		
			<tr>
				<th>Ref</th>
				<th>Produto</th>
				<th>Pre�o</th>
				<th>Categoria</th>
			</tr>
		</thead>
		<tbody>
		<!-- Mostra os produtos com JSTL -->
		<c:forEach items="${produtos}" var="produto">
			<tr>
				<td><c:out value="${produto.id}"></c:out></td>
				<td><c:out value="${produto.produto }"></c:out></td>
				<td><fmt:formatNumber value="${produto.preco}" type="currency"></fmt:formatNumber></td>
				<td><c:out value="${produto.categoria}"></c:out></td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</body>
</html>