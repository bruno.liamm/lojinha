package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import controle.UsuarioController;
import model.Usuario;


@WebServlet("/login")
public class UsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public UsuarioServlet() {
        super();
    }

    private RequestDispatcher dispacher(String forward) {
		return getServletContext().getRequestDispatcher("/" + forward);
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("logout");
		HttpSession session = request.getSession();
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		
		if(acao != null && usuario != null) {
			session.invalidate();
			dispacher("index.jsp").forward(request, response);
		}else {
			dispacher("index.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		
		UsuarioController usuarioController = new UsuarioController();		
		Usuario usuario = new Usuario();
		
		HttpSession session = request.getSession();
		
		if(!email.equals("") && !senha.equals("")) {
			usuario.setEmail(email);
			usuario.setSenha(senha);
			
			usuario = usuarioController.selecionarUsuario(usuario);
			
			if(usuario.getId() > 0) {
				request.setAttribute("erro", 0);
				
				session.setAttribute("usuario", usuario);
				
			}else {
				request.setAttribute("erro", 1);
			}
		}else {
			request.setAttribute("erro", 2);
		}
		
		dispacher("index.jsp").forward(request, response);
	}

}
