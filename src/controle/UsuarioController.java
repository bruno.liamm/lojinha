package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Usuario;

public class UsuarioController {
	public Usuario selecionarUsuario(Usuario usuario) {
		Connection con = null;
		try {
			con = new Conexao().abrir();
			String sql = "SELECT id, nome FROM user WHERE email=? and senha=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, usuario.getEmail());
			ps.setString(2, usuario.getSenha());
			
			ResultSet rs = ps.executeQuery();
			if(rs != null && rs.next()) {
				usuario.setId(rs.getInt("id"));
				usuario.setNome(rs.getString("nome"));
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			new Conexao().fechar(con);
		}
		return usuario;
	}

}
