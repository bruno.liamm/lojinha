package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public final class Conexao{

	public Connection abrir() {
			Connection c = null;
			try {
					Class.forName("com.mysql.jdbc.Driver");
					String banco = "lojinha";
					String servidor = "jdbc:mysql://localhost/" + banco+"?useTimezone=true&serverTimezone=UTC";
					String usr = "root";
					String pass = "root";
					c = DriverManager.getConnection(servidor,usr,pass);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return c;
	}
	
		public void fechar(Connection c) {
			try {
					c.close();
			} catch (SQLException e) {
					System.out.println(e.getMessage());
		}
	}

}

