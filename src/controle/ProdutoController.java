package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Produto;

public class ProdutoController {
	public List<Produto> selecionarPodutos() {
		Connection con = null;
		List<Produto> produtos = new ArrayList<Produto>();
		try {
			con = new Conexao().abrir();
			String sql = "SELECT id, produto, preco, categoria FROM produto";
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Produto produto = new Produto();
				produto.setCategoria(rs.getString("categoria"));
				produto.setId(rs.getInt("id"));
				produto.setPreco(rs.getFloat("preco"));
				produto.setProduto(rs.getString("produto"));
				produtos.add(produto);
			}
			rs.close();
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			new Conexao().fechar(con);
		}
		return produtos;
	}
}
